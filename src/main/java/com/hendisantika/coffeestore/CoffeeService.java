package com.hendisantika.coffeestore;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

/**
 * Created by IntelliJ IDEA.
 * Project : coffee-store
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/07/18
 * Time: 07.22
 * To change this template use File | Settings | File Templates.
 */
@Service
public class CoffeeService {

    private final RestTemplate restTemplate;

    public CoffeeService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @HystrixCommand(fallbackMethod = "fallback", commandProperties = {
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "2000"),
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000")})
    public String coffees() {
        return this.restTemplate.getForObject(URI.create("http://localhost:9090/coffees"), String.class);
    }

    public String fallback() {
        return "Cappuccino";
    }
}